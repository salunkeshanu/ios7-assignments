//
//  PlayingDeck.m
//  Matchismo
//
//  Created by Inspeero Technologies on 06/12/13.
//  Copyright (c) 2013 Inspeero Technologies. All rights reserved.
//

#import "PlayingDeck.h"
#import "PlayingCard.h"

@implementation PlayingDeck

-(instancetype)init{
    //NSLog(@"PlayingCardDeck Initialized");
    if(self = [super init]){
    
        for(NSString* suit in [PlayingCard validSuits]){
            for(NSUInteger rank=1; rank<[PlayingCard maxRank];rank++){
                PlayingCard* card = [[PlayingCard alloc]init];
                card.rank = rank;
                card.suit = suit;
                [self addCard:card];
            }
        }
    }
    return self;
}

@end
