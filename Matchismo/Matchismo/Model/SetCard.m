//
//  SetCard.m
//  Matchismo
//
//  Created by Inspeero Technologies on 10/12/13.
//  Copyright (c) 2013 Inspeero Technologies. All rights reserved.
//

#import "SetCard.h"
#import "objc/runtime.h"

@implementation SetCard

//Implements Superclass getter
-(NSAttributedString*) contents{
    NSMutableString* content = [[NSMutableString alloc]init];
    
    // number and character
    for(int i=0;i<self.number;i++){
      [content appendString:self.shape];
    }
    
    //stroke set
    NSNumber *stroke;
    CGFloat alpha = 1.0f;
    if ([self.stroke isEqualToString:@"solid"]) {
        stroke = [NSNumber numberWithFloat:-15];
    } else if([self.stroke isEqualToString:@"blank"]){
        stroke = [NSNumber numberWithFloat:15];
    }else if([self.stroke isEqualToString:@"striped"]){
        stroke = [NSNumber numberWithFloat:-15];
        alpha = 0.4;
    }
    
    NSDictionary *attrib = @{
                                 NSStrokeColorAttributeName:[[UIColor performSelector:NSSelectorFromString(self.color)]colorWithAlphaComponent:alpha],
                                 NSForegroundColorAttributeName: [[UIColor performSelector:NSSelectorFromString(self.color)] colorWithAlphaComponent:alpha],
                                 NSStrokeWidthAttributeName: stroke,
                                 };
    return [[NSAttributedString alloc]initWithString:content attributes:attrib];
}

// THE RULES OF SET: The cards must satisfy ALL these conditions:
//They all have the same number, or they have three different numbers.
//They all have the same symbol, or they have three different symbols.
//They all have the same shading, or they have three different shadings.
//They all have the same color, or they have three different colors.

#define MATCH_BONUS 5
- (int) match:(NSArray *)otherCards{
    
    int score = 0;
    BOOL isMatch =YES;
    
    //NSMutableDictionary *matchStats = [[NSMutableDictionary alloc]init];
    
    unsigned int outCount, i;
    objc_property_t *properties = class_copyPropertyList([SetCard class], &outCount);
    
    //All the match parameters are stored in a dictionary
    for (i=0; i<outCount; i++) {
        NSString *thisProperty = [ NSString stringWithUTF8String:property_getName(properties[i])];
        
        //Enter first property into mutable array
        id myObj;
        NSMutableArray* tempArray = [[NSMutableArray alloc]init];
        myObj = [self valueForKey:thisProperty];
        if ([myObj isKindOfClass:[NSString class]]) {
            [tempArray addObject:myObj];
        }else if([myObj isKindOfClass:[NSNumber class]]){
            [tempArray addObject:[NSString stringWithFormat:@"%@",myObj]];
        }
        
        for(SetCard *otherCard in otherCards){
            myObj = [otherCard valueForKey:thisProperty];
            if ([myObj isKindOfClass:[NSString class]]) {
                [tempArray addObject:myObj];
            }else if([myObj isKindOfClass:[NSNumber class]]){
                 [tempArray addObject:[NSString stringWithFormat:@"%@",myObj]];
                 }
        }
        //NSLog(@"%@", tempArray);
        
       // NSLog(@"%d",[SetCard matchAParameter:tempArray]);
        BOOL newMatch = [SetCard matchAParameter:tempArray];
        isMatch = isMatch && newMatch;
        tempArray = nil;
        NSLog(@"Property: %@ IsMatch: %hhd",thisProperty,newMatch);
    }

    if(isMatch){
        score += MATCH_BONUS;
    }
    NSLog(@"SCORE: %d",score);
    return score;
}

+(BOOL) matchAParameter:(NSArray*) params{
//    int allSame = [SetCard allSame:params];
//    int alldiff = [SetCard allDifferent:params];
//    NSLog(@"All Same: %d OR All Different %d",allSame, alldiff);
    return ([SetCard allSame:params] || [SetCard allDifferent:params]);
}

+(BOOL) allSame:(NSArray*) params{
    BOOL match = NO;
    for (int i=1; i<[params count]-1; i++) {
        // if all same
        if (([params[i-1] isEqualToString:params[i]] && [params[i] isEqualToString:params[i+1]])) {
            match = YES;
        } else {
            return NO;
        }
    }
    return match;
}

+(BOOL) allDifferent:(NSArray*) params{
    
    for(int i=0;i<[params count]-1;i++){
        for(int j=i+1;j<[params count];j++){
            if([params[i] isEqualToString:params[j]]) {
                //NSLog(@"at i=%d, j=%d %@ = %@",i,j, params[i], params[j]);
                return NO;
            }
        }
    }
    return YES;
}

+ (NSArray*) validShapes{
    
    return @[@"●",@"▲",@"■"];
}

+(NSArray*) validColors{
    return @[@"redColor",@"blueColor",@"greenColor"];
}
+(NSArray*) validStroke{
    return @[@"solid",@"blank",@"striped"];
}
+(NSUInteger) maxValidNumber{
    return [[SetCard validColors]count];
}
@end
