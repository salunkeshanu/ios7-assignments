//
//  cardMatchingGame.h
//  Matchismo
//
//  Created by Inspeero Technologies on 07/12/13.
//  Copyright (c) 2013 Inspeero Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Deck.h"
#import "Card.h"

@interface cardMatchingGame : NSObject

- (instancetype) initWithCardCount:(NSUInteger) count
                        usingDeck:(Deck*) deck
              numberOfCardsToMatch:(int) number;
- (void) chooseCardAtIndex:(NSUInteger)index;
- (Card*) cardAtIndex:(NSUInteger)index;
@property (nonatomic, strong) NSString* userEvent;
@property (nonatomic, readonly) NSInteger score;
@end
