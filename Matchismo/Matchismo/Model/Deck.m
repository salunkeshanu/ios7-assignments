//
//  Deck.m
//  Matchismo
//
//  Created by Inspeero Technologies on 06/12/13.
//  Copyright (c) 2013 Inspeero Technologies. All rights reserved.
//

#import "Deck.h"

@interface Deck()
@property (strong, nonatomic) NSMutableArray* cards; //A Deck of Cards
@end

@implementation Deck

- (NSUInteger)cardCount{
    return [self.cards count];
}

-(NSMutableArray*) cards{
    if(!_cards) _cards = [[NSMutableArray alloc] init];
    return _cards;
}

-(void)addCard:(Card *)card{
    [self addCard:card atTop:NO];
}

-(void)addCard:(Card *)card atTop:(BOOL)atTop{
    if (atTop) {
        [self.cards insertObject:card
                     atIndex:0];
    }
    else{
        [self.cards addObject:card];
    }
}

-(Card *)drawRandomCard{

    Card* randomCard = nil;
    if([self.cards count]){
        unsigned index = arc4random() & [self.cards count]-1;
        randomCard = self.cards[index];
        [self.cards removeObjectAtIndex:index];
    }
    //NSLog(@"Draw Card %@", randomCard.contents);
    return randomCard;
}

@end
