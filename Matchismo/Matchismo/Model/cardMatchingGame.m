//
//  cardMatchingGame.m
//  Matchismo
//
//  Created by Inspeero Technologies on 07/12/13.
//  Copyright (c) 2013 Inspeero Technologies. All rights reserved.
//

#import "cardMatchingGame.h"
#define CLICK_PENALTY -2
#define MATCH_BONUS self.numberOfCardsToMatch

@interface cardMatchingGame()
@property (nonatomic, readwrite) NSInteger score;
@property (nonatomic, strong) NSMutableArray* cards; //of Card
@property (nonatomic) int numberOfCardsToMatch;
@end

@implementation cardMatchingGame

- (NSMutableArray *)cards{
    if(!_cards){ _cards = [[NSMutableArray alloc]init];}
    return _cards;
}

- (instancetype)initWithCardCount:(NSUInteger)count usingDeck:(Deck *)deck numberOfCardsToMatch:(int) number{

    //NSLog(@"Init CardMatchingGame %@", self);
    self = [super init];
    if (self) {
        for(int i=0; i< count; i++){
            Card* card = [deck drawRandomCard];
            if (card) {
                [self.cards addObject:card];
            } else {
                self=nil;
                break;
            }
        }
    }
    self.numberOfCardsToMatch = number;
    self.userEvent = @"New Game";
    return self;
}

- (void)chooseCardAtIndex:(NSUInteger)index{
    Card* card = self.cards[index];
    if(!card.isMatched){
        if (card.isChosen) {
            card.chosen = NO;
            //NSLog(@"Card already chosen. Unchoose it");
        } else {
            //NSLog(@"Number of self.cards: %d", [self.cards count]);
            // temporary array to store otherCards
            NSMutableArray* otherCards = [[NSMutableArray alloc]init];
            for(Card* otherCard in self.cards){
                //NSLog(@"%d isChosen: %hhd and isMatched:%hhd",[self.cards indexOfObject:otherCard], otherCard.isChosen, otherCard.isMatched);
                
                if (otherCard.isChosen && !otherCard.isMatched) {
                    [otherCards addObject:otherCard];
                    
                    if([otherCards count] >= self.numberOfCardsToMatch-1){
                        //NSLog(@"Othercards count >= self.numberOfCardsToMatch %d >= %d", [otherCards count], self.numberOfCardsToMatch);
                        int matchScore = [card match:otherCards];
                        if(matchScore){
                            int tempScore = abs(matchScore * MATCH_BONUS);
                            self.score+= tempScore;
                            card.matched = YES;
                            for (Card* c in otherCards){
                            c.matched = YES;
                            }
                            self.userEvent = [NSString stringWithFormat:@"Card Matched ! Score+%d",tempScore ];
                        }else{
                            for (Card* c in otherCards){
                                c.chosen = NO;
                            }
                            self.score += CLICK_PENALTY;
                            self.userEvent = [NSString stringWithFormat:@"Not a Match Click Penalty %d", CLICK_PENALTY ];
                        }
                    }
                }
            }
            card.chosen = YES;
        }
    }
}

- (Card *)cardAtIndex:(NSUInteger)index {

    return (index <= [self.cards count])?self.cards[index]:nil;
}
@end
