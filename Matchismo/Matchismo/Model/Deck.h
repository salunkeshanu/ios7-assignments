//
//  Deck.h
//  Matchismo
//
//  Created by Inspeero Technologies on 06/12/13.
//  Copyright (c) 2013 Inspeero Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Card.h"

@interface Deck : NSObject

-(void) addCard:(Card*)card atTop:(BOOL)atTop;
-(void) addCard:(Card*)card;
-(NSUInteger) cardCount;
-(Card*) drawRandomCard;
@end
