//
//  PlayingCard.m
//  Matchismo
//
//  Created by Inspeero Technologies on 06/12/13.
//  Copyright (c) 2013 Inspeero Technologies. All rights reserved.
//

#import "PlayingCard.h"

@implementation PlayingCard

//Implements Superclass getter
-(NSString*) contents{
    NSArray* rankStrings = [PlayingCard rankStrings];
    return [rankStrings[self.rank] stringByAppendingString:self.suit];
}

@synthesize suit = _suit;

- (int) match:(NSArray *)otherCards{
    int score = 0;
    BOOL suitMatch = YES;
    BOOL rankMatch = YES;
    
    for ( PlayingCard* otherCard in otherCards){
    
        if (![otherCard.suit isEqualToString:self.suit]) {
            suitMatch = NO;
        }
        if (! (otherCard.rank == self.rank)){
            rankMatch = NO;
        }
    }
    if (suitMatch){
        score += 1;
    }
    if (rankMatch){
        score +=4;
    }
    NSLog(@"Match Suit %hhd", suitMatch);
    NSLog(@"Match Rank %hhd", rankMatch);
    return score;
}

+(NSArray *)validSuits{
    return @[@"♣",@"♠",@"♥",@"♦"];
}

+(NSUInteger)maxRank{
    return [[PlayingCard rankStrings] count];
}

- (void)setSuit:(NSString *)suit{
    if ([[ PlayingCard validSuits] containsObject:suit]) {
        _suit = suit;
    }
}

-(NSString *)suit{
    return _suit ? _suit:@"?";
}

+(NSArray*) rankStrings{
    return @[@"?",@"A",@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"J",@"Q",@"K"];
}
@end
