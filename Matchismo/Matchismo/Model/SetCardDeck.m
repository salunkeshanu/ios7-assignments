//
//  SetCardDeck.m
//  Matchismo
//
//  Created by Inspeero Technologies on 10/12/13.
//  Copyright (c) 2013 Inspeero Technologies. All rights reserved.
//

#import "SetCardDeck.h"
#import "SetCard.h"

@implementation SetCardDeck

-(instancetype)init{
    if(self = [super init]){
       
        for(int i=1;i<=[SetCard maxValidNumber];i++){
            for(NSString* color in [SetCard validColors]){
            for(NSString* stroke in [SetCard validStroke]){
                for(NSString* shape in [SetCard validShapes]){
                    SetCard* card = [[SetCard alloc]init];
                    card.number = i;
                    card.color = color;
                    card.stroke = stroke;
                    card.shape = shape;
                    [card contents];
                    [self addCard:card];

                    }
                }
            }
        }
        
    }
    return self;
}

@end
