//
//  SetCard.h
//  Matchismo
//
//  Created by Inspeero Technologies on 10/12/13.
//  Copyright (c) 2013 Inspeero Technologies. All rights reserved.
//

#import "Card.h"

@interface SetCard : Card

// Parameters of each card
@property (nonatomic, strong) NSString* shape;
@property (nonatomic, strong) NSString* color;
@property (nonatomic, strong) NSString* stroke;
@property (nonatomic) NSUInteger number;

+ (NSArray*) validShapes;
+(NSArray*) validColors;
+(NSArray*) validStroke;
+(NSUInteger) maxValidNumber;
@end





