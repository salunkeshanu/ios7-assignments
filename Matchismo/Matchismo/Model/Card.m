//
//  Card.m
//  Matchismo
//
//  Created by Inspeero Technologies on 06/12/13.
//  Copyright (c) 2013 Inspeero Technologies. All rights reserved.
//

#import "Card.h"

@implementation Card

-(int) match:(NSArray *)otherCards{
    int score=0;
    for( Card* card in otherCards){
        if ([card.contents isEqualToString:self.contents]) {
            score++;
        }
    }
    return score;
}

@end
