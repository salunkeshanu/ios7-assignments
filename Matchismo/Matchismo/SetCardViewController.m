//
//  SetCardViewController.m
//  Matchismo
//
//  Created by Inspeero Technologies on 10/12/13.
//  Copyright (c) 2013 Inspeero Technologies. All rights reserved.
//

#import "SetCardViewController.h"
#import "SetCardDeck.h"

@implementation SetCardViewController

- (Deck*) createDeck{
    Deck* t = [[SetCardDeck alloc] init];
    return t;
}

- (IBAction)dealSetCardGame:(UIButton *)sender {
    NSLog(@"*************** New Game **********************");
    self.game = nil;
    [self game];
    [self UpdateUI];
}

- (IBAction)flipMe:(UIButton *)sender {
    int chosenButtonIndex = [self.cardButtons indexOfObject:sender];
    [self.game chooseCardAtIndex:chosenButtonIndex];
    [self performSelector:@selector(UpdateUI) withObject:sender afterDelay:0.0];
}

- (void)viewDidLoad{
    self.numberOfCardsToMatch = 3;
    [self game];
    [self UpdateUI];
}

- (void) UpdateUI{
    [super UpdateUI];
    for (UIButton* cardButton in self.cardButtons){
        int cardButtonIndex = [self.cardButtons indexOfObject:cardButton];
        Card* card = [self.game cardAtIndex:cardButtonIndex];
        [cardButton setAttributedTitle:[self AttributedTitleForCard:card]
                              forState:UIControlStateNormal];
        [cardButton setBackgroundImage:[self imageForCard:card]
                              forState:UIControlStateNormal];
        //cardButton.enabled = !(card.isChosen && !card.isMatched);
        [cardButton setHighlighted:card.isChosen];
        self.gameScore.text = [NSString stringWithFormat:@"SCORE: %d", self.game.score];
        self.actionStatus.text = self.game.userEvent;
    }
}
- (NSAttributedString*) AttributedTitleForCard:(Card*) card{
    return card.contents;
}

- (UIImage*) imageForCard:(Card*) card{
    
    return [UIImage imageNamed:@"cardfront"];
}
@end
