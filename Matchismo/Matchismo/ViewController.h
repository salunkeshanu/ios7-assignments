//
//  ViewController.h
//  Matchismo
//
//  Created by Inspeero Technologies on 06/12/13.
//  Copyright (c) 2013 Inspeero Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Deck.h"
#import "cardMatchingGame.h"

@interface ViewController : UIViewController
@property (strong, nonatomic) cardMatchingGame* game;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *cardButtons;
@property (weak, nonatomic) IBOutlet UILabel *gameScore;
@property (weak, nonatomic) IBOutlet UILabel *actionStatus;
@property (nonatomic) int numberOfCardsToMatch;

- (Deck*) createDeck; //abstract
- (void) UpdateUI;
- (NSString*) titleForCard:(Card*) card;

- (UIImage*) imageForCard:(Card*) card;
@end
