//
//  ViewController.m
//  Matchismo
//
//  Created by Inspeero Technologies on 06/12/13.
//  Copyright (c) 2013 Inspeero Technologies. All rights reserved.
//

#import "ViewController.h"
#import "Card.h"
#import "cardMatchingGame.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UITextView *statusMEssage;
@end

@implementation ViewController

- (IBAction)setGameMode:(UISegmentedControl *)sender {
    self.numberOfCardsToMatch = sender.selectedSegmentIndex + 2;
    NSLog(@"Number == %d",self.numberOfCardsToMatch);
    _game = nil;
    [self game];
    [self UpdateUI];
}

- (cardMatchingGame *)game{
    
    if(!_game){
        // By default, gameMode=3
        if(!self.numberOfCardsToMatch){
        self.numberOfCardsToMatch = 3;
        }
        _game = [[cardMatchingGame alloc]initWithCardCount:[self.cardButtons count]
                                                 usingDeck:[self createDeck]
                                      numberOfCardsToMatch:self.numberOfCardsToMatch];
    }
    return _game;
}

- (Deck*) createDeck{
    return nil;
}

- (NSString*) randomCard:(Deck*)fromDeck{
    Card* randomCard = [fromDeck drawRandomCard];
    return randomCard.contents;
}

- (IBAction)flipMe:(UIButton *)sender {
    NSLog(@"Superclass Flip");
}

- (void) UpdateUI{
// overridden
    if(!self.statusMEssage.text){
    self.statusMEssage.text = @"STATUS MESSAGES\n";
    }
    NSLog(@"Updating %@", self.statusMEssage.text);
    self.statusMEssage.text = [NSString stringWithFormat:@"%@ \n %@",self.statusMEssage.text, self.actionStatus];
    
}

- (NSString*) titleForCard:(Card*) card{
    //NSLog(@"Card Contents: %@ and isChosen%hhd",card.contents, card.isChosen);
    return card.isChosen? card.contents:@"";
}

- (UIImage*) imageForCard:(Card*) card{

    return [UIImage imageNamed:card.isChosen ? @"cardfront": @"cardback"];
}

@end
