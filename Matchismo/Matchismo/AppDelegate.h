//
//  AppDelegate.h
//  Matchismo
//
//  Created by Inspeero Technologies on 06/12/13.
//  Copyright (c) 2013 Inspeero Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
