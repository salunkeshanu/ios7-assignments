//
//  PlayingCardViewController.m
//  Matchismo
//
//  Created by Inspeero Technologies on 09/12/13.
//  Copyright (c) 2013 Inspeero Technologies. All rights reserved.
//

#import "PlayingCardViewController.h"
#import "PlayingDeck.h"

@interface PlayingCardViewController ()

@end

@implementation PlayingCardViewController

- (Deck*) createDeck{
    return [[PlayingDeck alloc] init];
}

- (IBAction)flipMe:(UIButton *)sender {
    NSLog(@"Subclass Flip");
    int chosenButtonIndex = [self.cardButtons indexOfObject:sender];
    [self.game chooseCardAtIndex:chosenButtonIndex];
    //NSLog(@"**** CHOSE BUTTON ***");
    [self UpdateUI];
}

- (void) UpdateUI{
    [super UpdateUI];
    NSLog(@"PlayingCard Update UI");
    for (UIButton* cardButton in self.cardButtons){
        int cardButtonIndex = [self.cardButtons indexOfObject:cardButton];
        Card* card = [self.game cardAtIndex:cardButtonIndex];
        [cardButton setTitle: [self titleForCard:card] forState:UIControlStateNormal];
        [cardButton setBackgroundImage:[self imageForCard:card] forState:UIControlStateNormal];
        cardButton.enabled = !card.isMatched;
        self.gameScore.text = [NSString stringWithFormat:@"SCORE: %d", self.game.score];
        self.actionStatus.text = self.game.userEvent;
    }
}
@end
